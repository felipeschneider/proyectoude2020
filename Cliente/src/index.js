import Phaser from "phaser";
import GameScene from "./game";

/* El primer parametro del constructor de GameScene indica el bando
  0 = Buque Fabrica China, 1= Pesquero China, 2 = Arg PatrulleroLigero, 3 = Arg Patrullero Pesado, 
  4 = Uru Patrullero Ligero, 5 = Uru Patrullero Pesado.
*/
let gameScene = new GameScene(5);

const config = {
  type: Phaser.AUTO,
  parent: "phaser-example",
  width: 800,
  height: 600,
  physics: 
  {
    default : 'arcade',
    debug : true
  },
  scene: gameScene
};

const game = new Phaser.Game(config);