import Phaser from "phaser";
import { throws } from "assert";

class GameScene extends Phaser.Scene
{
    constructor(side)
    {
        super("playGame");
        let patrullero;
        let input;
        let pointerDistanceX=0;
        let pointerDistanceY=0;
        this.bando = side;
    }

    createTileMap(tileHeight, tileWidth, canvasHeight, canvasWidth)
    {
        for(var i=0;i<canvasHeight/tileHeight;i++)
        {
            for(var j=0;j<canvasWidth/tileWidth;j++)
            {
            this.add.image(i*tileHeight,j*tileWidth,'waterTile');
            }
        }
    }

    preload() 
    {
        this.load.image('waterTile', 'src/assets/Img/Tiles/WaterBG_tile.jpg');
        this.load.image('patrulleroLigeroArg', 'src/assets/Img/patrullero_ligero_arg.png');
        this.load.image('patrulleroLigeroUru', 'src/assets/Img/patrullero_ligero_uru.png');
        this.load.image('patrulleroPesadoArg', 'src/assets/Img/patrullero_pesado_arg.png');
        this.load.image('patrulleroPesadoUru', 'src/assets/Img/patrullero_pesado_uru.png');
    }

    create() 
    {
        let tileImg = this.add.image(0,0,'waterTile');
        let tileHeight = tileImg.height;
        let tileWidth = tileImg.width;
        let canvasHeight = this.sys.game.canvas.height;
        let canvasWidth = this.sys.game.canvas.width;
        console.log("Altura canvas: " + canvasHeight);
        console.log("Ancho canvas: " + canvasWidth);
        console.log("Altura tile: " + tileHeight);
        console.log("Ancho tile: " + tileWidth);

        this.physics.world.setBounds(0,0, 800, 600);
        
        for(var i=0;i<canvasWidth/tileWidth;i++)
        {
            for(var j=0;j<canvasHeight/tileHeight;j++)
            {
                this.add.image(i*tileHeight,j*tileWidth,'waterTile');
            }
        }
        //createTileMap(tileHeight, tileWidth, canvasHeight, canvasWidth);
        
        var gfx = this.add.graphics().setDefaultStyles({ lineStyle: { width: 10, color: 0xffdd00, alpha: 0.5 } });

        this.input.mouse.disableContextMenu();
        console.log("bando: " + this.bando);
        switch(this.bando)
        {
            case 2: this.patrullero = this.physics.add.image(0, 0, 'patrulleroLigeroArg');
                    break;

            case 3: this.patrullero = this.physics.add.image(0, 0, 'patrulleroPesadoArg');
                    break;
            
            case 4: this.patrullero = this.physics.add.image(0, 0, 'patrulleroLigeroUru')
                    break;

            case 5: this.patrullero = this.physics.add.image(0, 0, 'patrulleroPesadoUru')
                    break;
            
            default: this.patrullero = this.physics.add.image(0, 0, 'patrulleroLigeroArg');
                     break;
        }
        
        this.patrullero.setScale(0.35);
        this.patrullero.setPosition(this.patrullero.displayWidth/2,this.patrullero.displayHeight/2);
        this.patrullero.setCollideWorldBounds(true);

        this.input.on('pointerdown', function (pointer) 
        {
            this.pointerDistanceY = pointer.y;
            this.pointerDistanceX = pointer.x;
            if(pointer.rightButtonDown())
            {
                let angulo = Phaser.Math.Angle.BetweenPoints(this.patrullero, pointer);
                this.patrullero.rotation = angulo;
                this.physics.moveToObject(this.patrullero, pointer, 120);
            }
        }, this);        

    }

    update()
    {
        // Si la distancia entre el barco y el lugar donde se hizo click para mover es menor a 1, se detiene el barco
        if(Phaser.Math.Distance.Between(this.patrullero.x, this.patrullero.y, this.pointerDistanceX, this.pointerDistanceY) < 1)
        {
            this.patrullero.body.setVelocity(0,0);
        }

        // Estos chequeos son para cuando el barco toca uno de los bordes de la pantalla
        if(this.patrullero.body.onWall())
        {
            this.patrullero.body.setVelocity(0,0);
        }
        if(this.patrullero.body.onFloor())
        {
            this.patrullero.body.setVelocity(0,0);
        }
        if(this.patrullero.body.onCeiling())
        {
            this.patrullero.body.setVelocity(0,0);
        }
    }
}

export default GameScene;