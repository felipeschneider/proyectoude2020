  var MongoClient = require('mongodb').MongoClient;
  var url = 'mongodb://localhost:27017/ShipWar';

  /*********CREAR BASE DE DATOS*********/
  MongoClient.connect(url, function(err, client) {
      if (err) throw err;
      console.log("Database created!");
      client.close();
  });

  /*********CREAR COLLECCION*********/
  MongoClient.connect(url, function(err, client) {
      if (err) throw err;
      var db = client.db("ShipWar");
      db.createCollection("customers", function(err, res) {
          if (err) throw err;
          console.log("Collection created!");
          client.close();
      });
  });

  /* var item = {
       _id: 4,
       Name: "Test4",
       Time: "2:30",
       Date: Date(),
       Players: [{
           Name: "Manolo",
           Side: "Chino",
           PosicionX: 45,
           PosicionY: 90,
           Boat: {
               Tipo: "Pesquero",
               Combustible: 25,
               Vida: 95,
               CargaPescados: 63
           }
       }]
   };*/

  /*********INSERTAR UN NUEVO JUEGO*********/
  /*MongoClient.connect(url, function(err, client) {
      var db = client.db('ShipWar');
      db.collection('Game').insertOne(item, function(err, res) {
          if (err) throw err;
          console.log('Game saved');
          client.close();
      })

  });*/

  /*********BORRA UN JUEGO ESPECIFICO*********/
  /*const query = { Name: 'Test2' }
  MongoClient.connect(url, function(err, client) {
      var db = client.db('ShipWar');
      db.collection('Game').deleteOne(query, function(err, res) {
          if (err) throw err;
          console.log('Game deleted');
          client.close();
      })

  });*/

  /*********DEVUELVE TODOS LOS JUEGOS*********/
  /*MongoClient.connect(url, function(err, client) {
      var db = client.db('ShipWar');
      db.collection('Game').find({}).toArray(function(err, res) {
          if(err) throw err;
          console.log(JSON.stringify(res));
          client.close();
      })
  });*/

  /*********DEVUELVE UN JUEGO ESPECIFICO*********/
  MongoClient.connect(url, function(err, client) {
      var db = client.db('ShipWar');
      db.collection('Game').findOne({ "_id": 4 }, function(err, res) {
          console.log(JSON.stringify(res));
          client.close();
      })
  });