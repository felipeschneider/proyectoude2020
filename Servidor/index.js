/*
    Server Index para el Juego. Establece un WebSocket el el puerto 8083.
    Desde un cliente usando javascript, se puede establecer la comunicación
    a través del siguiente código:

    let ws = new WebSocket("ws://localhost:8083"); // Establece la conexión
    ws.onmessage = message => console.log(`${message.data}`); // Escucha respuesta del server
    ws.send("Hola Mundo"); // Envía mensaje al servidor
*/

const http = require("http");
const WebSocket = require("ws");
const express = require("express");
const Player = require("./models/player").default;
const GameService = require("./services/gameService")
const EventEnum = require("./enums/eventEnums");

let app = express();

// Web Socket
const wss = new WebSocket.Server({ port: 8083 });

wss.on('connection', function connection (ws) {

  ws.on('message', function incoming (payload) {

    var request = JSON.parse(payload);

    switch (request.event) {
      case EventEnum.chinaMoveShip.value:
        break;
      case EventEnum.chinaFishing.value:
        break;
      case EventEnum.chinaShipHealth.value:
        break;
      case EventEnum.chinaReplyRequest.value:
        break;
      case EventEnum.uruMoveShip.value:
        break;
      case EventEnum.uruRequestResponse.value:
        break;
      case EventEnum.uruFuel.value:
        break;
      case EventEnum.uruFires.value:
        break;
      case EventEnum.uruWeaponLoad.value:
        break;
      case EventEnum.gameTime.value:
        break;
      case EventEnum.uruWins.value:
        break;
      case EventEnum.chinaWins.value:
        break;
      default:
        break;
    }

    // Envia mensaje a todos menos a él mismo.
    wss.clients.forEach(function each (client) {

      if (request.event == 1) { // mover barco RdlP
        client.send("Se mueve barco RdlP");
      }
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        client.send(data);
      }
    });

  });

});

console.log("** WebSocket escuchando **");

// Rest API
app.use(express.json());

app.get("/", function (req, res) {
  res.send("Hello world");
});

// Crear nuevo juego en bd.
app.post("/game", function (req, res) {

  let game = new GameService();
  game.newGame(req.body);
});

// Guardar juego en bd.
app.put("/game", function (req, res) {
  let game = new GameService();

  // Hay jugador disponible?

  game.newGame(req.body);
});


http.createServer(app).listen(8082, function () {
  console.log("** Api escuchando **");
});
