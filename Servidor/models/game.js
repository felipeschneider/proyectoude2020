class Game {
    constructor(players, limit, shoal, time) {
        this.players = players; // Lista de jugadores.
        this.limit = limit; // Límite marítimo
        this.shoal = shoal; // Cardumen
        this.time = time; // Tiempo de juego
    }
}

module.exports = Game