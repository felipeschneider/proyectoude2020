class Player {
    constructor(id, name, side)
    {
        this.id = id; // Identificador de la sesión del jugador.
        this.name = name; // Nombre de jugador.
        this.side = side; // Bando del jugador (China o Rioplatense).
    }
}

module.exports = Player