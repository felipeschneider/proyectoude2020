const GameData = require("../persistance/gameData");

class GameService {
    newGame(game) {
        const data = new GameData();
        data.newGame(game);
    }
    gameWaitingExists() {
        const data = new GameData();
        return data.gameWaitingExists();
    }
}

module.exports = GameService