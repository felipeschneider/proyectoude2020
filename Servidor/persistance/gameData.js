var uuid = require('uuid');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/deadfish";

class GameData {

    newGame (game) {

        this.gameWaitingExists();

        MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true, })
            .then((db) => {
                var dbo = db.db("deadfish");

                // Crea la colección si no existe.
                dbo.createCollection("games", function (err, res) {
                    if (err) throw err;
                    console.log("Collection created!");
                });


                // Inserta el documento.

                var myobj = { gameId: uuid.v1() };
                dbo.collection("games").insertOne(myobj, function (err, res) {
                    if (err) throw err;

                    console.log("1 document inserted");
                    db.close();

                });
            })
            .catch(err => {
                console.log(err.message);
            });
    }

    gameWaitingExists () {

        MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true, })
        .then((db) => {
            var dbo = db.db("deadfish");

            // Crea la colección si no existe.
            dbo.createCollection("games", function (err, res) {
                if (err) throw err;
                console.log("Collection created!");
            });


            dbo.collection("games").find().toArray(function(err, docs) {
                if (err) throw err;

                console.log(docs[0].gameId);
                db.close();

            });
        })
        .catch(err => {
            console.log(err.message);
        });
    }

}

module.exports = GameData