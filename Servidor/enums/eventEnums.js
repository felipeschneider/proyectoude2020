const Enum = require('enum');

var eventsEnum = new Enum({
    'chinaMoveShip': 1,
    'chinaFishing': 2,
    'chinaShipHealth': 3,
    'chinaReplyRequest': 4,
    'uruMoveShip': 5,
    'uruRequestResponse': 6,
    'uruFuel': 7,
    'uruFires': 8,
    'uruWeaponLoad': 9,
    'gameTime': 10,
    'uruWins': 11,
    'chinaWins': 12
});

module.exports = eventsEnum