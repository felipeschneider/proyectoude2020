# Proyecto UDE 2020

## Cliente
Para poder editar y ejecutar el código del juego se deben seguir los siguientes pasos: 

 1. Tener instalado Visual Studio Code ([https://code.visualstudio.com/download](https://code.visualstudio.com/download)) y Node.js (versión LTS, [https://nodejs.org/es/download/](https://nodejs.org/es/download/)). 
 2. Haber hecho un branch propio desde el branch "staging". 
 3. Desde VSCode abrir una Terminal (ctrl+shift+ñ o menu superior Terminal->New Terminal). 
 4. En la terminal ir hasta la carpeta del repo y posicionarse dentro de Cliente 
 5. Una vez posicionadoes en \Cliente, escribimos en la terminal "npm install" 
 6. Luego de que instale las dependencias necesarias, ejecutar "npm start". Esto inicia el proceso batch que corre para compilar el código en tiempo real, también abrirá un navegador con el html del juego (el mismo se recarga con cada ctrl+s que hagamos guardando los cambios del código).

 ## Servidor
 Para poder editar y ejecutar el código del servidor, se deben seguir los siguientes pasos:

 1. Seguir los pasos 1, 2 y 3 de la sección "Cliente".
 2. En la terminal, ir hasta la carpeta del repo y posicionarse dentro de Servidor.
 3. Una vez allí, correr "npm install"
 4. Instalar mongodb. Por defecto lo instalará en mongodb://localhost:27017.
 4. Para correr el WebSocket y la api, en la terminal ejecutar "node index.js"
 5. En caso de querer realizar dubugging, en VS Code, ejecutar el código con F5.